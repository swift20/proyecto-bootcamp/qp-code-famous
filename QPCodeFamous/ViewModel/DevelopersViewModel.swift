//
//  ModelDevelopersViewModel.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 25/11/21.
//

import Foundation

class DevelopersViewModel {
    
//    public static var shared = DevelopersViewModel() //nos sirve para ingresar tanto a datos modelos como al fetch,
    
    //var datosModelo = Modelo(data: [])
    var datosModelo = [User?]()
    
    
    // En nuestra funcion fetch , usamos completion, para que  recargamos la tabla de forma manual, con esto cuando llamemos a fetch, vamos a poder poner un closure, el clousure es basicamente lo que esta entre las llaves, podemos poner entre las llaves alguna accion que se genere cuando se ejecute la funcion, y lo que queremos que se genere es que se recargue la tabla
    
    func fetch(completion: @escaping (_ done:Bool) -> Void){
        guard let url = URL(string: "https://qpcodefamous-default-rtdb.firebaseio.com/data.json") else { return  }
        
        URLSession.shared.dataTask(with: url){data,_,_ in
            
            guard let data = data else { return }
            do{
                let json = try JSONDecoder().decode([User?].self, from: data)
                DispatchQueue.main.async {
                    self.datosModelo = json
                    print(self.datosModelo)
                    completion(true)
                }
            }catch let error as NSError{
                print("Error en el json", error.localizedDescription)
            }
            
        }.resume() //es importante el resume(), ya que es el que ejecuta el dataTask
    }
}
