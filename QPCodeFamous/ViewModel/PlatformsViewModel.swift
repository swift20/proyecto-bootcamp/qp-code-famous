//
//  PlatformsViewModel.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 1/12/21.
//

import Foundation
class PlatformsViewModel {
    
//    public static var shared = PlatformsViewModel() //nos sirve para ingresar tanto a datosModelo como al fetch
    
    var datosModelo = [Platforms?]()

    func fetch(completion: @escaping (_ done:Bool) -> Void){
        guard let url = URL(string: "https://fir-uikit-1a340-default-rtdb.firebaseio.com/data.json") else { return  }
        
        URLSession.shared.dataTask(with: url){data,_,_ in
            
            guard let data = data else { return }
            do{
                let json = try JSONDecoder().decode([Platforms?].self, from: data)
                DispatchQueue.main.async {
                    self.datosModelo = json
                    print(self.datosModelo)
                    completion(true)
                }
            }catch let error as NSError{
                print("Error en el json", error.localizedDescription)
            }
            
        }.resume() //es importante el resume(), ya que es el que ejecuta el dataTask
    }
}
