//
//  PerfilViewModel.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 3/12/21.
//

import Foundation
import Firebase

class PerfilViewModel {
    
    public static let shared = PerfilViewModel()
    var datos = [UserModel]()
    
    // GUARDAR
    func save(name:String, profession: String, desc:String, role: String, secondidiom:Bool, linkedin: String, twitter : String, youtube: String, portfolio: String, gitlab : String, github: String,photo:UIImage, completion: @escaping (_ done: Bool) -> Void ){
        
        let storage = Storage.storage().reference()
        let nombrePortada = UUID()
        let directorio = storage.child("userimagenes/\(nombrePortada)")
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        directorio.putData(photo.jpegData(compressionQuality : 0.1)!, metadata: metadata){data,error in
            if error == nil {
                print("guardo la imagen")
                // GUARDAR TEXTO
                let db = Firestore.firestore()
                let id = UUID().uuidString
                guard let idUser = Auth.auth().currentUser?.uid else { return  }
                guard let email = Auth.auth().currentUser?.email else { return  }
                let campos : [String:Any] = ["name":name,"profession": profession,"desc":desc,"role":role,"secondidiom":secondidiom ,"linkedin":linkedin,"twitter":twitter,"youtube":youtube,"portfolio":portfolio,"gitlab":gitlab,"github":github ,"photo":String (describing: directorio),"idUser":idUser,"email":email]
                db.collection("user").document(id).setData(campos){error in
                    if let error = error?.localizedDescription{
                        print("error al guardar en firestore", error)
                    }else{
                        print("guardo todo")
                        completion(true)
                    }
                }
                
                // TERMINO DE GUARDAR TEXTO
            }else{
                if let error = error?.localizedDescription {
                    print("fallo al subir la imagen en storage", error)
                }else{
                    print("fallo la app")
                }
            }
        }
        
    }
    
    // mostrar
    
    func getData(completion: @escaping (_ done: Bool) -> Void){
            let db = Firestore.firestore()
            db.collection("user").addSnapshotListener { (QuerySnapshot, error) in
                if let error = error?.localizedDescription{
                    print("error al mostrar datos ", error)
                }else{
                    self.datos.removeAll()
                    for document in QuerySnapshot!.documents {
                        let valor = document.data()
                        let id = document.documentID
                        let email = valor["email"] as? String ?? "sin email"
                        let name = valor["name"] as? String ?? "sin name"
                        let profession = valor["profession"] as? String ?? "sin profession"
                        let desc = valor["desc"] as? String ?? "sin desc"
                        let secondidiom = valor["secondidiom"] as? Bool ?? false
                        let linkedin = valor["linkedin"] as? String ?? "sin linkedin"
                        let twitter = valor["twitter"] as? String ?? "sin twitter"
                        let youtube = valor["youtube"] as? String ?? "sin youtube"
                        let portfolio = valor["portfolio"] as? String ?? "sin portfolio"
                        let gitlab = valor["gitlab"] as? String ?? "sin gitlab"
                        let github = valor["github"] as? String ?? "sin github"
                        let photo = valor["photo"] as? String ?? "sin photo"
                        let role = valor["role"] as? String ?? "sin role"
                        DispatchQueue.main.async {
                            let registros = UserModel(id: id, email: email, name: name, desc: desc, profession: profession, role: role, secondidiom: secondidiom,linkedin:linkedin,twitter:twitter, youtube:youtube,portfolio:portfolio,gitlab:gitlab, github:github, photo: photo)
                            self.datos.append(registros)
                            completion(true)
                        }
                    }
                }
            }
            
        }
    
    // EDITAR
    
        func edit(name:String, profession: String, desc:String, role: String, secondidiom:Bool, linkedin: String, twitter : String, youtube: String, portfolio: String, gitlab : String, github: String,id:String, completion: @escaping (_ done:Bool) -> Void ){
            let db = Firestore.firestore()
            let campos : [String:Any] = ["name":name,"profession":profession,"desc":desc,"role": role, "secondidiom":secondidiom,"linkedin":linkedin,"twitter":twitter,"youtube":youtube,"portfolio":portfolio,"gitlab":gitlab,"github":github]
            db.collection("user").document(id).updateData(campos){error in
                if let error = error?.localizedDescription {
                    print("Error al editar", error)
                }else{
                    print("edito solo texto")
                    completion(true)
                }
            }
        }
        
        // EDITAR CON IMAGEN
        func editWithImage(name:String, profession: String, desc:String, role: String, secondidiom:Bool, linkedin: String, twitter : String, youtube: String, portfolio: String, gitlab : String, github: String,id:String, index:UserModel, photo: UIImage, completion: @escaping (_ done:Bool) -> Void ){
            // Eliminar imagen
            let imagen = index.photo
            let borrarImagen = Storage.storage().reference(forURL: imagen)
            borrarImagen.delete(completion: nil)
            
            // Subir la nueva imagen
            
            let storage = Storage.storage().reference()
            let nombrePortada = UUID()
            let directorio = storage.child("userimagenes/\(nombrePortada)")
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            directorio.putData(photo.jpegData(compressionQuality : 0.1)!, metadata: metadata){data,error in
                if error == nil {
                    print("guardo la imagen nueva")
                    // EDITANDO TEXTO
                    let db = Firestore.firestore()
                    let campos : [String:Any] = ["name":name,"profession":profession,"desc":desc, "secondidiom":secondidiom,"linkedin":linkedin,"twitter":twitter,"youtube":youtube, "portfolio":portfolio,"gitlab":gitlab,"github":github, "photo":String(describing: directorio),"role": role]
                    db.collection("user").document(id).updateData(campos){error in
                        if let error = error?.localizedDescription {
                            print("Error al editar", error)
                        }else{
                            print("edito solo texto")
                            completion(true)
                        }
                    }
                    
                    // TERMINO DE GUARDAR TEXTO
                }else{
                    if let error = error?.localizedDescription {
                        print("fallo al subir la imagen en storage", error)
                    }else{
                        print("fallo la app")
                    }
                }
            }
        }
}
