//
//  Radius.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 26/11/21.
//

import Foundation
import UIKit

extension UIView {
    func setRadius(radius: CGFloat? =  nil, width: Double = 3.0, opacityy: Double = 0.6){
        self.layer.cornerRadius =  radius ?? self.frame.width / 2
        self.layer.masksToBounds = true
        
        self.layer.borderColor = UIColor(red:0.42,green: 0.34, blue: 1.00, alpha: opacityy).cgColor
        self.layer.borderWidth = width
    }
    
}

//