//
//  Shadow.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 23/11/21.
//

import UIKit

extension UIView{
    var borderUIColor: UIColor{
        get{
            guard let color = layer.borderColor else{
                return UIColor.black
            }
            return UIColor(cgColor: color)
        }
        set{
            layer.borderColor = newValue.cgColor
        }
    }
}
