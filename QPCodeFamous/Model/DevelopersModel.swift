//
//  ModelDevelopers.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 25/11/21.
//

import Foundation

struct Modelo: Decodable {
    
//    let code: Int?
//    let meta: Meta?
    let data: [User?]
    
}

struct User: Decodable {
    let id: Int
    let name: String
    let work: String
    let image: String
    let category: String
    let calificacion: String
    let cursos: String
    let estudiantes: String
    let email: String
    let descripcion: String
    let status: String
    let technology1: String
    let technology2: String
    let technology3: String
    let udemy: String?
    let web: String?
    let platzi: String?
    let twitter: String?
    let youtube: String?
    let linkedin: String?
    let podcast: String?
}

