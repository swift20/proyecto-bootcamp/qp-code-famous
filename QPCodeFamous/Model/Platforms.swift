//
//  Platforms.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 1/12/21.
//

import Foundation

struct PlatformsModelo: Decodable {
    let data: [Platforms?]
}

struct Platforms: Decodable {
    let id: Int
    let name: String
    let image: String
    let logo: String
    let calificacion: String
    let estudiantes: String
    let instructores: String
    let cursos: String
    let descripcion: String
    let web: String?
    let Fundador1: String
    let Fundador1Image: String
    let Fundador2: String?
    let Fundador2Image: String?
    let Fundador3: String?
    let Fundador3Image: String?
}

