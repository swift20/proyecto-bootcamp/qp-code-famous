//
//  FirebaseModel.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 28/11/21.
//

import Foundation
struct PostModel {
    var id : String
    var email : String
    var titulo : String
    var desc : String
    var portada : String
    var tecnologias : String
}
