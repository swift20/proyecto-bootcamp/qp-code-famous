//
//  UsuarioModel.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 3/12/21.
//

import Foundation

struct UserModel {
    var id : String
    var email : String
    var name : String
    var desc : String
    var profession :  String
    var role: String
    var secondidiom: Bool
    var linkedin : String
    var twitter: String
    var youtube: String
    var portfolio : String
    var gitlab: String
    var github: String
    var photo : String
}
