//
//  RegisterViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 28/11/21.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func registrar(_ sender: UIButton) {
        guard let emailtxt = email.text else { return  }
        guard let passtxt = password.text else { return }
        PostViewModel.shared.createUser(email: emailtxt, pass: passtxt) { (done) in
            UserDefaults.standard.set(true, forKey: "sesion")
            self.next(identificador: "registrarLogin")
        }
    }
    func next(identificador: String){
        performSegue(withIdentifier: identificador, sender: self)
    }
    
    
}
