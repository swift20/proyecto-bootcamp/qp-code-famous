//
//  LoginViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 28/11/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var gmail: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //lo hicimos en viewDidAppear, porque no funciona el performance segue dentro del viewDidLoad
    
    //Esto me ayuda a mantener mi app , si es que ya ingrese
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if (UserDefaults.standard.object(forKey: "sesion") != nil ){
            self.next(identificador: "entrarLogin")
        }
    }
    
    @IBAction func ingresar(_ sender: UIButton) {
        guard let emailtxt = email.text else { return  }
        guard let passtxt = password.text else { return }
        PostViewModel.shared.login(email: emailtxt, pass: passtxt) { (done,Error) in
           
            if done {
                UserDefaults.standard.set(true, forKey: "sesion")
                //no pude hacer el performarce aqui porque estoy dentro de un closure, asi que se hizo con funcion
                self.next(identificador: "entrarLogin")
            }
            if Error == nil{
                self.showAlert()
            }

        }
    }
    func next(identificador: String){
        performSegue(withIdentifier: identificador, sender: self)
    }
    
    @IBAction func gmailAction(_ sender: Any) {
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Usuario Incorrecto", message: "Verifique que el usuario o contraseña este bien escrito o que exista", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Aceptar", style: .default) {(_) in
            print("Error login")
        }
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
        print("alerta")
    }
    

}
