//
//  onBoardingViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 24/11/21.
//

import UIKit

struct OnboardingItem{
    let title : String
    let description: String
    let imageName: String
}
class OnBoardingViewController: UIPageViewController {
    var pageControl: UIPageControl? // Puede que la otra vista no pase algo
    // lazy solo se ejecuta cuando lo usan
    //fileprivate es un apropiedad que solo puede ser accesible en este mismo archivo
    fileprivate(set) lazy var items:  [OnboardingItem] = {
        return [
            OnboardingItem(title: "Developers Movil", description: "Encuentra a desarrodores de tecnologías como Swift, Kotlin, Flutter, Ionic, React native.", imageName: "OnBoarding1"),
            OnboardingItem(title: "Developers Web", description: "Conoce las plataformas educativas reconocidas en desarrollo web con React, Vue, Angular.", imageName: "OnBoarding2"),
            OnboardingItem(title: "Developers IoT", description: "Si eres un profesional del rubro tecnologico, eres nuevo en programacion y quieres dar un enfoque con temas de IoT, Machine Learnig, este es el lugar indicado.", imageName: "OnBoarding3")
        ]
    }() //cuando el usuario llame a estos items los vamos a crear
    
    // Por cada item se debe crear un ViewController el content
    
    fileprivate(set) lazy var contentViewController:  [UIViewController] = {
        var items = [UIViewController]() //Asignamos una lista vacia de UIViewController
        for i in 0 ..< self.items.count {
            items.append(self.instanteViewController(i))
        }
        return items
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        delegate = self
        
        pageControl?.numberOfPages = items.count
        updateContainerView(stepNumber: 0)
        
    }
    
    func updateContainerView(stepNumber index: Int){
        setViewControllers([contentViewController[index]], direction: .forward, animated: true, completion: nil)
    }
    
    func instanteViewController(_ index: Int) -> UIViewController {
        guard let viewController = UIStoryboard(name: "OnBoarding", bundle: .main)
                .instantiateViewController(withIdentifier: "OnBoardingSteps") as? OnBoardingStepsViewController else{
                    return UIViewController()
                }
        //pasamos informacion a nuestro viewcontroller y lo retornamos
        viewController.item = items[index]
        return viewController
    }
}

extension OnBoardingViewController: UIPageViewControllerDataSource{
    //implementamos los metodos obligatorios
    
    //este es el que esta antes de nuestro viewController actual
    func pageViewController(
        _ pageViewController:UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
            let index = contentViewController.firstIndex(of: viewController) //preguntamos el indice del view controller
            if index == 0 { // posicion inicial y sino hay nadie despues de el, retornamos nil, porque es el unico elemento
                return nil
            }
            return contentViewController[index! - 1] // retornar esa posicion mas un elemento
        }
    
    //este es el que esta despues de nuestro viewController actual
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let index = contentViewController.firstIndex(of: viewController) //preguntamos el indice del view controller
        if index == contentViewController.count - 1{ // el index es el maximo,  y sino hay nadie despues de el retorna nil
            return nil
        }
        //si hay aun algo despues, retornar esa posicion mas un elemento
        return contentViewController[index! + 1]
    }
    
}

//Cuando un elemento pasa de una vista a otra debemos pasarle al pager, y decirle que ahora estamos en una nueva vista para que se actualice
extension OnBoardingViewController: UIPageViewControllerDelegate{
    // Este metodo es llamado cuando la transcion ha sido completada
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        //pedimos el primero que sabemos existe, porque lo estamos declarando
        guard let index = contentViewController.firstIndex(of: viewControllers!.first!) else {
            return
        }
        pageControl?.currentPage = index
    }
    
}
