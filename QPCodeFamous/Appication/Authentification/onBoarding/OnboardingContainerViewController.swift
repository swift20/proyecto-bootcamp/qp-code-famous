//
//  OnboardingContainerViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 24/11/21.
//

import UIKit

class OnboardingContainerViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "openOnBoarding",
              let destination = segue.destination as? OnBoardingViewController else{
                  return
              }
        destination.pageControl = pageControl
    }

}
