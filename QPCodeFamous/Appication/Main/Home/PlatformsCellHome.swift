
import UIKit
class PlatformCellHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagenPlat: UIImageView!
    @IBOutlet weak var namePlat: UILabel!
    @IBOutlet weak var star1Plat: UIImageView!
    @IBOutlet weak var star2Plat: UIImageView!
    @IBOutlet weak var star3Plat: UIImageView!
    @IBOutlet weak var star4Plat: UIImageView!
    @IBOutlet weak var Star5Plat: UIImageView!
    @IBOutlet weak var estudiantes: UILabel!
    @IBOutlet weak var logo: UIButton!
    @IBOutlet weak var calificacion: UILabel!
    @IBOutlet weak var progressPlatformHome: UIActivityIndicatorView!
}
