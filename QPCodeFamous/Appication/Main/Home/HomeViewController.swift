import UIKit
import Firebase
class DevsCellHome: UITableViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var work: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var starValor: UILabel!
    @IBOutlet weak var estudiantes: UILabel!
    @IBOutlet weak var progressDev: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imagen?.setRadius(radius: 20.0,opacityy: 0.8)
    }
}

class HomeViewController: UIViewController {

    
    @IBOutlet weak var imageProfileHome: UIImageView!
    @IBOutlet weak var nameProfileHome: UILabel!
    @IBOutlet weak var workProfileHome: UILabel!
    @IBOutlet weak var roleProfileHome: UIImageView!
    @IBOutlet weak var progressProfilehome: UIActivityIndicatorView!
    
    @IBOutlet weak var tablaHome: UITableView!
    @IBOutlet weak var progressList: UIActivityIndicatorView!
    
    @IBOutlet weak var collectionHome: UICollectionView!
    @IBOutlet weak var progressCollection: UIActivityIndicatorView!
    
    var listUser = [UserModel?]()
    var filteredUser = [UserModel?]()
    var datosUser: UserModel?
    var developsData = DevelopersViewModel()
    var platformsData = PlatformsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressProfilehome.hidesWhenStopped = true
        self.imageProfileHome?.setRadius( width: 3.0, opacityy: 1.0)
        self.roleProfileHome?.setRadius( width: 3.0, opacityy: 1.0)
        
        tablaHome.delegate = self
        tablaHome.dataSource = self
        progressList.startAnimating()
        progressList.hidesWhenStopped = true
        
        developsData.fetch { done in
            if done { //si funciona hacer ..
                self.progressList.stopAnimating()
                self.progressList.isHidden = true
                self.tablaHome.reloadData()
            }
        }
        collectionHome.delegate = self
        collectionHome.dataSource = self
        progressCollection.startAnimating()
        progressCollection.hidesWhenStopped = true
        platformsData.fetch { done in
            if done { //si funciona hacer ..
                self.progressCollection.stopAnimating()
                self.progressCollection.isHidden = true
                self.collectionHome.reloadData()
                print("aqui")
            }
        }
        
        PerfilViewModel.shared.getData { done in
            if done {

                let userEmail = Auth.auth().currentUser?.email
                self.listUser = PerfilViewModel.shared.datos
                print(self.listUser.count)
                
                self.filteredUser = self.listUser.filter
                {
                    dev in
                    let scopeMatch = ( dev!.email.lowercased().contains(userEmail!))
                    if(scopeMatch)
                    {
                        return true
                    }
                    else{
                        return false
                    }
                }
                
                print("Aqui profile \(self.filteredUser.count)")
                
                if ( self.filteredUser.count != 0 ) {
                    self.progressProfilehome.isHidden = false
                    self.progressProfilehome.startAnimating()
                    self.datosUser = self.filteredUser[0]
                    self.nameProfileHome.text = self.datosUser?.name
                    self.workProfileHome.text = self.datosUser?.profession
                    self.roleProfileHome.backgroundColor = UIColor(named: "PrimaryColor")
                    if self.datosUser?.role == "Estudiante" {
                        self.roleProfileHome.image = UIImage(systemName: "graduationcap.circle.fill")
                    }else{
                        self.roleProfileHome.image = UIImage(systemName: "bag.circle.fill")
                    }
                    Storage.storage().reference(forURL: self.datosUser!.photo).getData(maxSize: 10 * 1024 * 1024) { (data, error) in
                        if let error = error?.localizedDescription {
                            print("error al traer la imagen", error)
                        }else{
                            DispatchQueue.main.async {
                                
                                self.imageProfileHome.image = UIImage(data: data!)
                                self.progressProfilehome.stopAnimating()
                                self.progressProfilehome.isHidden = true
                                print("traer imagen")
                            }
                        }
                    }
                }
            }
        }
        

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "devenviarHome"{
            if let id = tablaHome.indexPathForSelectedRow{
                let   fila = developsData.datosModelo[id.row]
                let destino = segue.destination as? DevDetalleViewController
                destino?.datoslista = fila
            }

        }
        if segue.identifier == "platformenviarHome"{
            if let id = collectionHome.indexPathsForSelectedItems!.last{
       
                print(id[1])
                let fila  = platformsData.datosModelo[id[1]]
                let destino2 = segue.destination as? PlatformDetalleViewController
                destino2?.platformlista = fila
            }
        }
    }
    
}

extension HomeViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return developsData.datosModelo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tablaHome.dequeueReusableCell(withIdentifier: "cellDevHome", for: indexPath) as! DevsCellHome
        let   user = developsData.datosModelo[indexPath.row]
        cell.progressDev.startAnimating()

        cell.nombre.text = user?.name
        cell.work.text = user?.work
        cell.starValor.text = user?.calificacion
        cell.estudiantes.text = user?.estudiantes
        cell.star1.image = UIImage(systemName: "star.fill")
        cell.star2.image = UIImage(systemName: "star.fill")
        cell.star3.image = UIImage(systemName: "star.fill")
        cell.star4.image = UIImage(systemName: "star.fill")
        
        let valor = Double((user?.calificacion)!) ?? 5.0
        switch valor  {
        case 3.6...4.0:
            cell.star5.image = UIImage(systemName: "star")
        case 4.1...4.5:
            cell.star5.image = UIImage(systemName: "star.leadinghalf.fill")
        case 4.6...5.0:
            cell.star5.image = UIImage(systemName: "star.fill")
        default:
            cell.star5.image = UIImage(systemName: "star")

        }
        
        guard let imageURL = URL(string: user?.image ?? "") else { fatalError("sin imagen") }
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                cell.progressDev .stopAnimating()
                cell.progressDev.isHidden = true
                cell.imagen.image = image
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "devenviarHome", sender: self)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "devenviarHome" {
//            if let id = tablaHome.indexPathForSelectedRow{
//                let   fila = DevelopersViewModel.shared.datosModelo.data[id.row]
//                let destino = segue.destination as? DevDetalleViewController
//                destino?.datoslista = fila
//            }
//        }
//    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(platformsData.datosModelo.count)
        return platformsData.datosModelo.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellHome = collectionView.dequeueReusableCell(withReuseIdentifier: "platfomCellHome", for: indexPath) as! PlatformCellHomeCollectionViewCell
        let platform = platformsData.datosModelo[indexPath.row]
        
        cellHome.contentView.layer.cornerRadius = 20.0
        cellHome.contentView.layer.borderWidth = 1.0
        cellHome.contentView.layer.borderColor =  UIColor.clear.cgColor
        cellHome.contentView.layer.masksToBounds =  true
        
        cellHome.layer.shadowColor =  UIColor.gray.cgColor
        cellHome.layer.shadowOffset =  CGSize(width: 0, height: 1.0)
        cellHome.layer.shadowOpacity = 1.0
        cellHome.layer.masksToBounds = false
        cellHome.layer.shadowPath = UIBezierPath(roundedRect: cellHome.bounds,cornerRadius: cellHome.contentView.layer.cornerRadius).cgPath
        
        
        cellHome.progressPlatformHome.startAnimating()
        cellHome.progressPlatformHome.hidesWhenStopped = true
        
        //Seccion para traer la imagen
        guard let imageURL = URL(string: platform?.image ?? "") else { fatalError("sin imagen") }
        // es global para que encierre todo el proceso que hace para transformar la imagen
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                cellHome.progressPlatformHome.stopAnimating()
                cellHome.progressPlatformHome.isHidden = true
                cellHome.imagenPlat.image = image
                //self.tabla.reloadData() //recargamos la tabla , lo comente porque recargaba multiples veces la tabla
            }
        }
        //Seccion para traer la imagen 2
        guard let imageURL2 = URL(string: platform?.logo ?? "") else { fatalError("sin imagen") }
        DispatchQueue.global().async {
            guard let imageData2 = try? Data(contentsOf: imageURL2) else { return }
            let image2 = UIImage(data: imageData2)
            DispatchQueue.main.async {
//                cell.logo.imageView?.contentMode = .scaleAspectFit
                cellHome.logo.setImage(image2, for: .normal)
            }
        }
        cellHome.namePlat.text = platform?.name
        cellHome.calificacion.text = platform?.calificacion
        cellHome.estudiantes.text = platform?.estudiantes
        cellHome.star4Plat.image = UIImage(systemName: "star.fill")
        
        let valor = Double((platform?.calificacion)!) ?? 5.0
        switch valor  {
        case 3.6...4.0:
            cellHome.Star5Plat.image = UIImage(systemName: "star")

        case 4.1...4.5:
            cellHome.Star5Plat.image = UIImage(systemName: "star.leadinghalf.fill")
        case 4.6...5.0:
            cellHome.Star5Plat.image = UIImage(systemName: "star.fill")
        default:
            cellHome.Star5Plat.image = UIImage(systemName: "star")
        }
        
        return cellHome
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath)
        performSegue(withIdentifier: "platformenviarHome", sender: self)
    }

    
    
}
