//
//  AddProfileViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 5/12/21.
//

import UIKit
import Firebase

class AddProfileViewController: UIViewController {

    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var profession: UITextField!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var role: UIPickerView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var linkedin: UITextField!
    @IBOutlet weak var twitter: UITextField!
    @IBOutlet weak var youtube: UITextField!
    @IBOutlet weak var portfolio: UITextField!
    @IBOutlet weak var gitlab: UITextField!
    @IBOutlet weak var github: UITextField!
    
    @IBOutlet weak var progressPhoto: UIActivityIndicatorView!
    
    
    @IBOutlet weak var secondidiom: UISwitch!
    @IBOutlet weak var progressactivity: UIActivityIndicatorView!
    
    var dataUser: UserModel?
    
    var image = UIImage()
    let roles = ["Estudiante","Profesional"]
    var consola = "Estudiante"
    var ingles = false
    override func viewDidLoad() {
        super.viewDidLoad()
        progressactivity.isHidden =  true
        progressPhoto.startAnimating()
        progressPhoto.hidesWhenStopped = true
        role.delegate = self
        role.dataSource = self
        secondidiom.onTintColor = UIColor(named: "PrimaryColor")
        secondidiom.isOn = false
        
        
        if dataUser == nil {
            self.title = "Agregar User"
        }else{
            self.title = "Editar User"
        }
        
        name.text = dataUser?.name
        profession.text = dataUser?.profession
        desc.text = dataUser?.desc
        linkedin.text = dataUser?.linkedin
        twitter.text = dataUser?.twitter
        youtube.text = dataUser?.youtube
        portfolio.text = dataUser?.portfolio
        gitlab.text = dataUser?.gitlab
        github.text = dataUser?.github
        secondidiom.isOn = dataUser?.secondidiom ?? false
        ingles = dataUser?.secondidiom ?? false
        consola = dataUser?.role ?? "Estudiante"
        guard let valor = roles.firstIndex(of: consola) else { return }
        role.selectRow( valor , inComponent: 0, animated: true)
        
        if dataUser?.photo != nil {
            Storage.storage().reference(forURL: dataUser?.photo ?? "").getData(maxSize: 10 * 1024 * 1024) { (data, error) in
                if let error = error?.localizedDescription {
                    print("error al traer la imagen", error)
                }else{
                    DispatchQueue.main.async {
                        self.progressPhoto.stopAnimating()
                        self.progressPhoto.isHidden = true
                        self.photo.image = UIImage(data: data!)
                        self.image = UIImage(data: data!)! // para editar con imagen , y reusar la imagen que enviamos
                    
                    }
                }
            }
        }
        else{
            self.progressPhoto.stopAnimating()
            self.progressPhoto.isHidden = true
        }
        
        
    }
    
    
    @IBAction func secondidiomAction(_ sender: UISwitch) {
        if secondidiom.isOn{
            ingles = true
        }else{
            ingles = false
        }
    }
    @IBAction func loadImageAction(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }

    
    @IBAction func saveAction(_ sender: UIButton) {
        progressactivity.isHidden = false
        progressactivity.startAnimating()
        guard let name = name.text else {return}
        guard let profession = profession.text else {return}
        guard let descri = desc.text else {return}
        guard let linkedin =  linkedin.text else {return}
        guard let twitter = twitter.text else {return}
        guard let youtube = youtube.text else {return}
        guard let portfolio =  portfolio.text else {return}
        guard let gitlab = gitlab.text else {return}
        guard let github = github.text else {return}

            if dataUser == nil {
                PerfilViewModel.shared.save(name: name, profession: profession, desc: descri, role: consola, secondidiom: ingles, linkedin: linkedin, twitter: twitter, youtube: youtube,
                                            portfolio: portfolio, gitlab: gitlab, github: github,photo: image) { done in
                    if done {
                        
                        self.name.text = ""
                        self.profession.text = ""
                        self.desc.text = ""
                        self.linkedin.text = ""
                        self.twitter.text = ""
                        self.youtube.text = ""
                        self.portfolio.text = ""
                        self.gitlab.text = ""
                        self.github.text = ""
                        self.photo.image = UIImage(systemName: "person.crop.circle.fill")
                        self.navigationController?.popViewController(animated: true)
                        self.progressactivity.stopAnimating()
                        self.progressactivity.isHidden = true
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                if photo.image == UIImage(systemName: "person.crop.circle.fill") {
                    PerfilViewModel.shared.edit(name: name, profession: profession, desc: descri, role: consola, secondidiom: ingles, linkedin: linkedin, twitter: twitter, youtube: youtube,portfolio: portfolio, gitlab: gitlab, github: github, id: dataUser!.id) { (done) in
                        if done {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }else{
                    PerfilViewModel.shared.editWithImage(name: name, profession: profession, desc: descri, role: consola, secondidiom: ingles, linkedin: linkedin, twitter: twitter, youtube: youtube,portfolio: portfolio, gitlab: gitlab, github: github, id: dataUser!.id, index: dataUser!, photo: image) { (done) in
                        if done {
                     
 
                        
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                    }
                }
            }
            
        }
    }
    



extension AddProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return roles[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        consola =  roles[row]
        print(consola)
    }
}

extension AddProfileViewController: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    //cuando damos en cancel al queres seleccionar una imagen
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //caundo ya tomamos la imagen
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let imagenTomada = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        image = imagenTomada!
        photo.image = image
        dismiss(animated: true, completion: nil)
    }
    
}
