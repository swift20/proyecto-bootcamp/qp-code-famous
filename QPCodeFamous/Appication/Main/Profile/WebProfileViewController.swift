//
//  WebProfileViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 7/12/21.
//

import UIKit
import WebKit

class WebProfileViewController: UIViewController {

    @IBOutlet weak var myWebProfile: WKWebView!
    
    var linkprofile: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
    
        myWebProfile.load(URLRequest(url: URL(string: linkprofile)!))
    }
}


