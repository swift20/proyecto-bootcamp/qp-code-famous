//
//  ProfileViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 29/11/21.
//

import UIKit
import Firebase
class ProfileViewController: UIViewController {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profession: UILabel!
    @IBOutlet weak var role: UIImageView!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var secondidiom: UILabel!
    
    @IBOutlet weak var gitlab: UIButton!
    
    @IBOutlet weak var portfolio: UIButton!
    @IBOutlet weak var github: UIButton!
    
    @IBOutlet weak var linkedin: UIButton!
    @IBOutlet weak var youtube: UIButton!
    @IBOutlet weak var twitter: UIButton!
    @IBOutlet weak var progressProfile: UIActivityIndicatorView!
    @IBOutlet weak var espanol: UILabel!
    
    var listUser = [UserModel?]()
    var filteredUser = [UserModel?]()
    var datosUser: UserModel?
    var valorlink: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        progressProfile.hidesWhenStopped = true
        self.photo?.setRadius( width: 3.0, opacityy: 1.0)
        self.role?.setRadius( width: 3.0, opacityy: 1.0)

        PerfilViewModel.shared.getData { done in
            if done {
                
                let userEmail = Auth.auth().currentUser?.email
                
                self.listUser = PerfilViewModel.shared.datos
                print(self.listUser.count)
                
                self.filteredUser = self.listUser.filter
                {
                    dev in
                    let scopeMatch = ( dev!.email.lowercased().contains(userEmail!))
                    if(scopeMatch)
                    {
                        return true
                    }
                    else{
                        return false
                    }
                }
                
                print("Aqui \(self.filteredUser.count)")
                
                
                if ( self.filteredUser.count != 0 ) {
                    self.progressProfile.isHidden = false
                    self.progressProfile.startAnimating()

                    self.datosUser = self.filteredUser[0]
                    
                    self.name.text = self.datosUser?.name
                    self.profession.text = self.datosUser?.profession
                    self.email.text = self.datosUser?.email
                    self.desc.text = self.datosUser?.desc
                    self.espanol.isHidden = true
                    
                    
                    if self.datosUser?.secondidiom == true {
                        self.espanol.isHidden = false
                        self.secondidiom.isHidden = false
                    }
                    else{
                        self.espanol.isHidden = false
                        self.secondidiom.isHidden = true
                    }
                    
                    self.role.backgroundColor = UIColor(named: "PrimaryColor")
                    if self.datosUser?.role == "Estudiante" {
                        self.role.image = UIImage(systemName: "graduationcap.circle.fill")
                    }else{
                        self.role.image = UIImage(systemName: "bag.circle.fill")
                    }
                    
                    self.portfolio.isHidden = false
                    self.gitlab.isHidden = false
                    self.github.isHidden = false
                    self.linkedin.isHidden = false
                    self.youtube.isHidden = false
                    self.twitter.isHidden = false
                    
                    if self.datosUser?.gitlab == "" {
                        self.gitlab.isHidden = true
                    }
                    if self.datosUser?.portfolio == "" {
                        self.portfolio.isHidden = true
                    }
                    if self.datosUser?.github == "" {
                        self.github.isHidden = true
                    }
                    if self.datosUser?.linkedin == "" {
                        self.linkedin.isHidden = true
                    }
                    if self.datosUser?.youtube == "" {
                        self.youtube.isHidden = true
                    }
                    if self.datosUser?.twitter == "" {
                        self.twitter.isHidden = true
                    }
                    
                    Storage.storage().reference(forURL: self.datosUser!.photo).getData(maxSize: 10 * 1024 * 1024) { (data, error) in
                        if let error = error?.localizedDescription {
                            print("error al traer la imagen", error)
                        }else{
                            DispatchQueue.main.async {
                                
                                self.photo.image = UIImage(data: data!)
                                self.progressProfile.stopAnimating()
                                self.progressProfile.isHidden = true
                                print("traer imagen")
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "editarProfile"{
            let destino = segue.destination as! AddProfileViewController
            destino.dataUser = self.datosUser
        }
        if segue.identifier == "profileLink"{
            let destino2 = segue.destination as? WebProfileViewController
            destino2?.linkprofile = valorlink
            print(valorlink)
        }
    }
    

    
    @IBAction func editarAction(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "editarProfile", sender: self)
    }
    
    @IBAction func salir(_ sender: Any) {
        try! Auth.auth().signOut()
        UserDefaults.standard.removeObject(forKey: "sesion")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gitlabAction(_ sender: UIButton) {
        guard let gitlablink = datosUser?.gitlab else { return }
        valorlink = gitlablink
        performSegue(withIdentifier: "profileLink", sender: self)
    }
    @IBAction func portfolioAction(_ sender: UIButton) {
        guard let portfoliolink = datosUser?.portfolio else { return }
        valorlink = portfoliolink
        performSegue(withIdentifier: "profileLink", sender: self)
    }
    @IBAction func githubAction(_ sender: UIButton) {
        guard let githublink = datosUser?.github else { return }
        valorlink = githublink
        performSegue(withIdentifier: "profileLink", sender: self)
    }
    @IBAction func linkedinAction(_ sender: UIButton) {
        guard let linkedinlink = datosUser?.linkedin else { return }
        valorlink = linkedinlink
        performSegue(withIdentifier: "profileLink", sender: self)
    }
    @IBAction func youtube(_ sender: UIButton) {
        guard let youtubelink = datosUser?.youtube else { return }
        valorlink = youtubelink
        performSegue(withIdentifier: "profileLink", sender: self)
    }
    @IBAction func twitter(_ sender: UIButton) {
        guard let twitterlink = datosUser?.twitter else { return }
        valorlink = twitterlink
        performSegue(withIdentifier: "profileLink", sender: self)
    }
}

