//
//  PlatformDetalleViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 1/12/21.
//

import UIKit

class PlatformDetalleViewController: UIViewController {
    @IBOutlet weak var portada: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var calificacion: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var instructores: UILabel!
    @IBOutlet weak var estudiantes: UILabel!
    @IBOutlet weak var cursos: UILabel!
    @IBOutlet weak var descripcion: UITextView!
    @IBOutlet weak var progressPortada: UIActivityIndicatorView!
    
    
    var platformlista : Platforms?
    var valorlink: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        print(platformlista ?? "")
        progressPortada.startAnimating()
        progressPortada.hidesWhenStopped = true
        name.text = platformlista?.name
        calificacion.text = platformlista?.calificacion
        instructores.text = platformlista?.instructores
        estudiantes.text = platformlista?.estudiantes
        cursos.text =  platformlista?.cursos
        descripcion.text = platformlista?.descripcion
        
        
        logo?.setRadius( width: 3.0, opacityy: 1.0)
        
        guard let imageURL = URL(string: platformlista?.image ?? "") else { fatalError("sin imagen") }
        
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let imagen = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.progressPortada.stopAnimating()
                self.progressPortada.isHidden = true
                self.portada.image = imagen
            }
        }
        guard let imageURL2 = URL(string: platformlista?.logo ?? "") else { fatalError("sin imagen") }
        
        DispatchQueue.global().async {
            guard let imageData2 = try? Data(contentsOf: imageURL2) else { return }
            let imagen2 = UIImage(data: imageData2)
            DispatchQueue.main.async {
     
                self.logo.image = imagen2
            }
        }
        
        let valor = Double((platformlista?.calificacion)!) ?? 5.0
        switch valor  {
        case 3.6...4.0:
            star4.image = UIImage(systemName: "star.fill")
            star5.image = UIImage(systemName: "star")

        case 4.1...4.5:
            star4.image = UIImage(systemName: "star.fill")
            star5.image = UIImage(systemName: "star.leadinghalf.fill")
        case 4.6...5.0:
            star4.image = UIImage(systemName: "star.fill")
            star5.image = UIImage(systemName: "star.fill")
        default:
            star4.image = UIImage(systemName: "star")
            star5.image = UIImage(systemName: "star")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "enviarlinkplatform"{
            let destino = segue.destination as? WebLinlkPlatformViewController
            destino?.link = valorlink
            print(valorlink)
        }
    }

    @IBAction func weblLink(_ sender: UIButton) {
        
        guard let platformlink = platformlista?.web else { return }
        valorlink = platformlink
        performSegue(withIdentifier: "enviarlinkplatform", sender: self)
        print(valorlink)
    }
}
