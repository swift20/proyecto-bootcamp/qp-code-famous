//
//  WebLinlkPlatformViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 2/12/21.
//

import UIKit
import WebKit

class WebLinlkPlatformViewController: UIViewController {
    @IBOutlet weak var myWebPlatformView: WKWebView!
    
    var link: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        myWebPlatformView.load(URLRequest(url: URL(string: link)!))
    }
    


}
