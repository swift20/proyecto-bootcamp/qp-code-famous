//
//  PlatformCellCollectionViewCell.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 1/12/21.
//

import UIKit

class PlatformCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var logo: UIButton!
    @IBOutlet weak var estudiantes: UILabel!
    @IBOutlet weak var calificacion: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var recommendation: UIView!
    
    @IBOutlet weak var progressPlatfInfo: UIActivityIndicatorView!
}
