//
//  PlatformsViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 1/12/21.
//platformcell

import UIKit
import Firebase

class PlatformsViewController: UIViewController {

    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var progressPlatforms: UIActivityIndicatorView!
    
    var listUser = [UserModel?]()
    var filteredUser = [UserModel?]()
    var datosUser: UserModel?
    
    var role = ""
    var secondidioma = false
    
    var platformsData = PlatformsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        progressPlatforms.startAnimating()
        self.progressPlatforms.isHidden = false
        collection.delegate = self
        collection.dataSource = self
        platformsData.fetch { done in
            if done { //si funciona hacer ..
                self.progressPlatforms.stopAnimating()
                self.progressPlatforms.isHidden = true
//                self.collection.reloadData()
            }
        }
        
        PerfilViewModel.shared.getData { done in
            if done {
                
                let userEmail = Auth.auth().currentUser?.email
                self.listUser = PerfilViewModel.shared.datos
                print(self.listUser.count)
                
                self.filteredUser = self.listUser.filter
                {
                    dev in
                    let scopeMatch = ( dev!.email.lowercased().contains(userEmail!))
                    if(scopeMatch)
                    {
                        return true
                    }else{
                        return false
                    }
                }
                if ( self.filteredUser.count != 0 ) {
                    self.datosUser = self.filteredUser[0]
                    self.role = self.datosUser!.role
                    self.secondidioma = self.datosUser!.secondidiom
                    print(self.role)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                  self.collection.reloadData()
                    }
                    
                }
            }
        }
        
    }
    
    @IBAction func load(_ sender: UIButton) {
        self.collection.reloadData()
    }
}

extension PlatformsViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(platformsData.datosModelo.count)
        return platformsData.datosModelo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "platformcell", for: indexPath) as! PlatformCellCollectionViewCell
        let platform = platformsData.datosModelo[indexPath.row]
        
        cell.contentView.layer.cornerRadius = 20.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor =  UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds =  true
        
        cell.layer.shadowColor =  UIColor.gray.cgColor
        cell.layer.shadowOffset =  CGSize(width: 0, height: 1.0)
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds,cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        
        cell.progressPlatfInfo.startAnimating()
        cell.progressPlatfInfo.hidesWhenStopped = true
        
        //Seccion para traer la imagen

        guard let imageURL = URL(string: platform?.image ?? "") else { fatalError("sin imagen") }
        guard let imageURL2 = URL(string: platform?.logo ?? "") else { fatalError("sin imagen") }

        // es global para que encierre todo el proceso que hace para transformar la imagen
        DispatchQueue.global().async{
            let imageData = try! Data(contentsOf: imageURL)
            let imageData2 = try! Data(contentsOf: imageURL2)
            let image = UIImage(data: imageData)
            let image2 = UIImage(data: imageData2)

            DispatchQueue.main.async {

                cell.progressPlatfInfo.stopAnimating()
                cell.progressPlatfInfo.isHidden = true
                cell.image.image = image
                cell.logo.setImage(image2, for: .normal)

                //self.collection.reloadData() //recargamos la tabla , lo comente porque recargaba multiples veces la tabla
            }
        }

        cell.name.text = platform?.name
        cell.calificacion.text = platform?.calificacion
        cell.estudiantes.text = platform?.estudiantes
        
        let valor = Double((platform?.calificacion)!) ?? 5.0
        switch valor  {
        case 3.6...4.0:
            cell.star5.image = UIImage(systemName: "star")
        case 4.1...4.5:
            cell.star5.image = UIImage(systemName: "star.leadinghalf.fill")
        case 4.6...5.0:
            cell.star5.image = UIImage(systemName: "star.fill")
        default:
            cell.star5.image = UIImage(systemName: "star")
        }
        
        cell.recommendation.isHidden = true
        print(indexPath)
        
        
        if indexPath == [0,0]{
            if self.role == "Profesional" && self.secondidioma == true {
                cell.recommendation.isHidden = false
            }
        }
        if indexPath == [0,1]{
            if self.role == "Estudiante" || self.role == "Profesional" {
                cell.recommendation.isHidden = false
            }
        }
        if indexPath == [0,2]{
            if self.role == "Profesional" && self.secondidioma == true{
                cell.recommendation.isHidden = false
            }
        }
        if indexPath == [0,3]{
            if self.role == "Estudiante"{
                cell.recommendation.isHidden = false
            }
        }
        if indexPath == [0,4]{
            if self.role == "Profesional"{
                cell.recommendation.isHidden = false
            }
        }
        if indexPath == [0,5]{
            if self.role == "Estudiante" && self.secondidioma == true {
                cell.recommendation.isHidden = false
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath)
        performSegue(withIdentifier: "platformenviar", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "platformenviar" {
            if let id = collection.indexPathsForSelectedItems!.last{
       
                print(id[1])
                let fila  = platformsData.datosModelo[id[1]]
                let destino = segue.destination as? PlatformDetalleViewController
                destino?.platformlista = fila
            }

        }
    }
}


