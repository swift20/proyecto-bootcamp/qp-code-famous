//
//  WebLinkViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 30/11/21.
//

import UIKit
import WebKit

class WebLinkViewController: UIViewController {
    @IBOutlet weak var myWebView: WKWebView!
    
    var link: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        myWebView.load(URLRequest(url: URL(string: link)!))
    }
    

}
