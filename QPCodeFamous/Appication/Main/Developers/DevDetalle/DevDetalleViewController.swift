import UIKit

class DevDetalleViewController: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descripcion: UITextView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var work: UILabel!
    
    @IBOutlet weak var califica: UILabel!
    @IBOutlet weak var estudiantes: UILabel!
    @IBOutlet weak var calificacion: UILabel!
    @IBOutlet weak var cursos: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var udemy: UIButton!
    @IBOutlet weak var web: UIButton!
    @IBOutlet weak var platzi: UIButton!
    @IBOutlet weak var linkedin: UIButton!
    @IBOutlet weak var youtube: UIButton!
    @IBOutlet weak var twitter: UIButton!
    
    
    var datoslista : User?
    var valorlink: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(datoslista?.name ?? "error")
        
        name.text = datoslista?.name
        work.text = datoslista?.work
        estudiantes.text = datoslista?.estudiantes
        calificacion.text = datoslista?.calificacion
        califica.text = datoslista?.calificacion
        cursos.text = datoslista?.cursos
        descripcion.text = datoslista?.descripcion
        
        image?.setRadius(width: 0.0)
        
        let valor = Double((datoslista?.calificacion)!) ?? 5.0
        switch valor  {
        case 3.6...4.0:
            star5.image = UIImage(systemName: "star")
        case 4.1...4.5:
            star5.image = UIImage(systemName: "star.leadinghalf.fill")
        case 4.6...5.0:
            star5.image = UIImage(systemName: "star.fill")
        default:
            star5.image = UIImage(systemName: "star")
        }
        
        if datoslista?.udemy == "" {
            udemy.isHidden = true
        }
        if datoslista?.web == "" {
            web.isHidden = true
        }
        if datoslista?.platzi == "" {
            platzi.isHidden = true
        }
        if datoslista?.linkedin == "" {
            linkedin.isHidden = true
        }
        if datoslista?.youtube == "" {
            youtube.isHidden = true
        }
        if datoslista?.twitter == "" {
            twitter.isHidden = true
        }
        
        //Seccion para traer la imagen
        guard let imageURL = URL(string: datoslista?.image ?? "") else { fatalError("sin imagen") }
        // es global para que encierre todo el proceso que hace para transformar la imagen
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let imagen = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.image.image = imagen
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "enviarlink"{
            let destino = segue.destination as? WebLinkViewController
            destino?.link = valorlink
            print(valorlink)
        }
    }
    
    @IBAction func udemyAction(_ sender: UIButton) {
        guard let udemylink = datoslista?.udemy else { return }
        valorlink = udemylink
        performSegue(withIdentifier: "enviarlink", sender: self)
    }
    @IBAction func webAction(_ sender: UIButton) {
        guard let weblink = datoslista?.web else { return }
        valorlink = weblink
        performSegue(withIdentifier: "enviarlink", sender: self)
    }
    @IBAction func platziAction(_ sender: UIButton) {
        guard let platzilink = datoslista?.platzi else { return }
        valorlink = platzilink
        performSegue(withIdentifier: "enviarlink", sender: self)
    }
    @IBAction func linkedin(_ sender: UIButton) {
        guard let linkedinlink = datoslista?.linkedin else { return }
        valorlink = linkedinlink
        performSegue(withIdentifier: "enviarlink", sender: self)
    }
    
    @IBAction func youtubeAction(_ sender: UIButton) {
        guard let youtubelink = datoslista?.youtube else { return }
        valorlink = youtubelink
        performSegue(withIdentifier: "enviarlink", sender: self)
    }
    @IBAction func twitterAction(_ sender: UIButton) {
        guard let twitterlink = datoslista?.twitter else { return }
        valorlink = twitterlink
        performSegue(withIdentifier: "enviarlink", sender: self)
    }
}
