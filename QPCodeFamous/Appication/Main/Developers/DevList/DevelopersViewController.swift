import UIKit

class celda: UITableViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var work: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var starValor: UILabel!
    @IBOutlet weak var estudiantes: UILabel!
    @IBOutlet weak var progressDev: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //creamos esta funcion en Extensions para el radio, sino mandamos nada , el resultado es circular
        imagen?.setRadius(radius: 20.0,opacityy: 0.8)
    }
}

class DevelopersViewController: UIViewController, UISearchBarDelegate,UISearchResultsUpdating {
    
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    //
    let searchController = UISearchController()
    var devsList = [User?]()
    var filteredDevs = [User?]()
    //
    var developsData = DevelopersViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabla.delegate = self
        tabla.dataSource = self
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        developsData.fetch { done in
            if done {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.tabla.reloadData() 
                self.devsList = self.developsData.datosModelo
            }
        }
        //
        initSearchController()
        //
    }
    
    //
    func initSearchController(){
        searchController.loadViewIfNeeded()
        searchController.searchBar.tintColor = UIColor(named: "PrimaryColor")
        searchController.searchResultsUpdater =  self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.enablesReturnKeyAutomatically = false
        searchController.searchBar.returnKeyType = UIReturnKeyType.done
        definesPresentationContext = true
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.scopeButtonTitles = ["All", "movil", "web","ioT"]
        searchController.searchBar.delegate = self
        
    }
 
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scopeButton = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        let searchText = searchBar.text!
        
        filterForSearchTextAndScopeButton(searchText: searchText,scopeButton: scopeButton)
        
    }
    
    func filterForSearchTextAndScopeButton(searchText: String, scopeButton: String = "All"){
        //print(searchText)
        print(scopeButton)
        filteredDevs = devsList.filter
        {
            dev in
            let scopeMatch = (scopeButton == "All" || dev!.category.lowercased().contains(scopeButton.lowercased()))
            if(searchController.searchBar.text != "")
            {
                let searchTextMatch = dev!.name.lowercased().contains(searchText.lowercased())
                return scopeMatch && searchTextMatch
            }
            else{
                return scopeMatch
            }
        }
        //print(filteredDevs)
        self.tabla.reloadData()
        
    }
    //
}

extension DevelopersViewController: UITableViewDelegate,UITableViewDataSource{
    
    //cantidad de filas que tendremos
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        if(searchController.isActive){
            return filteredDevs.count
        }
        //
        return developsData.datosModelo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        // Nota: celda viene de nuestra clase personalizada
        let cell = tabla.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! celda
        
        //
        let user: User?
        if(searchController.isActive)
        {
            user = filteredDevs[indexPath.row]
        }else{
            user = developsData.datosModelo[indexPath.row]
            cell.progressDev.startAnimating()
        }
        //
        
        cell.nombre.text = user?.name
        cell.work.text = user?.work
        cell.starValor.text = user?.calificacion
        cell.estudiantes.text = user?.estudiantes
        cell.star1.image = UIImage(systemName: "star.fill")
        cell.star2.image = UIImage(systemName: "star.fill")
        cell.star3.image = UIImage(systemName: "star.fill")
        cell.star4.image = UIImage(systemName: "star.fill")

        let valor = Double((user?.calificacion)!) ?? 5.0
        switch valor  {
        case 3.6...4.0:
            cell.star5.image = UIImage(systemName: "star")
        case 4.1...4.5:
            cell.star5.image = UIImage(systemName: "star.leadinghalf.fill")
        case 4.6...5.0:
            cell.star5.image = UIImage(systemName: "star.fill")
        default:
            cell.star5.image = UIImage(systemName: "star")
        }
        
        //Seccion para traer la imagen
        guard let imageURL = URL(string: user?.image ?? "") else { fatalError("sin imagen") }
        // es global para que encierre todo el proceso que hace para transformar la imagen
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                cell.progressDev .stopAnimating()
                cell.progressDev.isHidden = true
                cell.imagen.image = image
                //self.tabla.reloadData() //recargamos la tabla , lo comente porque recargaba multiples veces la tabla
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "devenviar", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "devenviar" {
            if let id = tabla.indexPathForSelectedRow{
        
                //
                let fila: User?
                if(searchController.isActive)
                {
                    fila = filteredDevs[id.row]
                }
                else{
                    fila = developsData.datosModelo[id.row]
                }
                //
                let destino = segue.destination as? DevDetalleViewController
                destino?.datoslista = fila
            }

        }
    }
    
}




