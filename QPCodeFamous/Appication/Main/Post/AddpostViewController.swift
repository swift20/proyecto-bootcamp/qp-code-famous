//
//  AddpostViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 29/11/21.
//

import UIKit
import Firebase
class AddpostViewController: UIViewController {

    @IBOutlet weak var titulo: UITextField!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var tecnologias: UIPickerView!
    @IBOutlet weak var portada: UIImageView!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    
    var datos : PostModel?

    var image = UIImage()
    
    let tec = ["Swift","Kotlin","Flutter","Angular","React","Vue","Nodejs","Python","Java","Php","SQL","Arduino","AppInventor"]
    var consola = "Swift"

    override func viewDidLoad() {
        super.viewDidLoad()
        progress.isHidden =  true
        tecnologias.delegate =  self
        tecnologias.dataSource =  self

        if datos == nil {
            self.title = "Agregar Post"
        }else{
            self.title = "Editar Post"
        }
        
        titulo.text = datos?.titulo
        desc.text = datos?.desc
        consola = datos?.tecnologias ?? "Swift"
        guard let valor = tec.firstIndex(of: consola) else { return }
        tecnologias.selectRow( valor , inComponent: 0, animated: true)
        
        if datos?.portada != nil {
            Storage.storage().reference(forURL: datos?.portada ?? "").getData(maxSize: 10 * 1024 * 1024) { (data, error) in
                if let error = error?.localizedDescription {
                    print("error al traer la imagen", error)
                }else{
                    DispatchQueue.main.async {
                        self.portada.image = UIImage(data: data!)
                        self.image = UIImage(data: data!)! // para editar con imagen , y reusar la imagen que enviamos
                    }
                }
            }
        }
    }
    
    @IBAction func cargarImagen(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary //aca elegimos si abrir la camara o galeria
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func publicar(_ sender: UIButton) {
        progress.isHidden = false
        progress.startAnimating()
        guard let title = titulo.text else { return }
        guard let descri = desc.text else { return  }
        
        if datos == nil {
            PostViewModel.shared.save(titulo: title, desc: descri, tecnologias: consola, portada: image) { (done) in
                if done {
                    self.titulo.text = ""
                    self.desc.text = ""
                    self.portada.image = UIImage(systemName: "photo.artframe")
                    self.progress.stopAnimating()
                    self.progress.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }else{
            if portada.image == UIImage(systemName: "photo.artframe") {
                PostViewModel.shared.edit(titulo: title, desc: descri, tecnologias: consola, id: datos!.id) { (done) in
                    if done {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                PostViewModel.shared.editWithImage(titulo: title, desc: descri, tecnologias: consola , id: datos!.id, index: datos!, portada: image) { (done) in
                    if done {
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
            }
        }
    }
    
}

extension AddpostViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    // columnas que podria tener el objeto
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    // numero de componenetes
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tec.count
    }
    //colar el titulo de los objetos
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return tec[row]
    }
    
    //que cuando demos un click haga tal cosa
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        consola = tec[row]
        print(consola)
    }
    
}

extension AddpostViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil) // se ejecuta cuando damos al boton de cancelar, se elimina la vista y se da un dismiss
    }

    //cuando ya tomamos la fotografia
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let imagenTomada = info [UIImagePickerController.InfoKey.editedImage] as? UIImage
        image = imagenTomada!

        //forma visual para saber que tomamos la imagen
        portada.image = image
        //para regresar a nuestra vista
        dismiss(animated: true, completion: nil)

    }
}

