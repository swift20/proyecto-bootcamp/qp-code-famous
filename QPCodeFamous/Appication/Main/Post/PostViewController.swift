//
//  PostViewController.swift
//  QPCodeFamous
//
//  Created by Jorge Teofanes Vicuña Valle on 28/11/21.
//

import UIKit
import Firebase

class CeldaPost: UITableViewCell{
    @IBOutlet weak var portada: UIImageView!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var tecnologia: UILabel!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var autor: UILabel!
    @IBOutlet weak var progreso: UIActivityIndicatorView!
        
}
class PostViewController: UIViewController {
    @IBOutlet weak var tabla: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabla.delegate = self
        tabla.dataSource = self
        PostViewModel.shared.getData{ (done) in
            if done {
                self.tabla.reloadData()
            }
        }
    }
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PostViewModel.shared.datos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabla.dequeueReusableCell(withIdentifier: "celdaposts", for: indexPath) as! CeldaPost
        //let valor = FirebaseViewModel.shared.datos.count - indexPath.row - 1
        let post = PostViewModel.shared.datos[indexPath.row ]
        cell.progreso.startAnimating()
        cell.progreso.hidesWhenStopped = true
        
        cell.titulo.text = post.titulo
        cell.desc.text = post.desc
        cell.tecnologia.text = post.tecnologias
        cell.autor.text = post.email
        //Diseno label tecnologia
        cell.tecnologia.layer.cornerRadius = 15.0
        cell.tecnologia.layer.masksToBounds = true

        Storage.storage().reference(forURL: post.portada).getData(maxSize: 10 * 1024 * 1024) { (data, error) in
            if let error = error?.localizedDescription {
                print("error al traer la imagen", error)
            }else{
                DispatchQueue.main.async {
                    cell.portada.image = UIImage(data: data!)
                    cell.progreso.stopAnimating()
                    cell.progreso.isHidden = true
                    print("traer imagen")
                    //self.tabla.reloadData() //esto producia que las imagenes se recarguen varias veces y la cosnulta se haga muchas veces
                }
            }
        }
        return cell
    }
    
    // editar

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userEmail = Auth.auth().currentUser?.email
        let post = PostViewModel.shared.datos[indexPath.row]
        if post.email == userEmail{
            print("soy el mismo")
            performSegue(withIdentifier: "editarPost", sender: self)
        }
        //performSegue(withIdentifier: "editarPost", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editarPost"{
            if let id = tabla.indexPathForSelectedRow {
                let fila = PostViewModel.shared.datos[id.row]
                let destino = segue.destination as! AddpostViewController
                destino.datos = fila
            }
            /* if let id = tabla.indexPathForSelectedRow {
                let fila = FirebaseViewModel.shared.datos[id.row]
                let destino = segue.destination as! AddpostViewController
                destino.datos = fila
            } */
        }
    }

    //Eliminar
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let userEmail = Auth.auth().currentUser?.email
        let post = PostViewModel.shared.datos[indexPath.row]
        
        if post.email == userEmail{
            print("soy el mismo")
            let delete = UIContextualAction(style: .destructive, title: "borrar") { (_, _, _) in
                PostViewModel.shared.delete(index: post)
            }
            delete.image = UIImage(systemName: "trash")
            return UISwipeActionsConfiguration(actions: [delete])
        }
        return UISwipeActionsConfiguration()
        
/*      let delete = UIContextualAction(style: .destructive, title: "borrar") { (_, _, _) in
            FirebaseViewModel.shared.delete(index: post)
        }
        delete.image = UIImage(systemName: "trash")
        return UISwipeActionsConfiguration(actions: [delete])
*/
    }

    
}


